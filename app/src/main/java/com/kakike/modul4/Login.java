package com.kakike.modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {

    private EditText email, pass;
    private Button login;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = mAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        if(mUser != null){
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        layout();
    }

    public void Daftar(View view) {
        Intent daftar = new Intent(this, Daftar_Akun.class);
        startActivity(daftar);
    }

    private void layout(){
        email = findViewById(R.id.Passwordx);
        pass = findViewById(R.id.Emailx);
        login = findViewById(R.id.Masuk);

    }


    public String pesan(String a){
        Toast.makeText(this, a, Toast.LENGTH_SHORT).show();
    return a;
    }

    public void masuk(View view) {

        if (validate()) {
            String user_email = email.getText().toString().trim();
            String user_password = pass.getText().toString().trim();

            mAuth.signInWithEmailAndPassword(user_email,user_password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                pesan("Login Berhasil");
                                Intent go = new Intent(Login.this, MainActivity.class);
                                startActivity(go);
                                finish();
                            }
                            else{
                                pesan("Login Gagal");
                            }
                        }
                    });



            /*mAuth.createUserWithEmailAndPassword(user_email, user_password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Toast.makeText(Login.this, "Login Berhasil", Toast.LENGTH_SHORT)
                                        .show();
                                startActivity(new Intent(Login.this, Lay_Menu.class));
                            }
                            else{
                                Toast.makeText(Login.this, "Login Gagal", Toast.LENGTH_SHORT)
                                        .show();

                            }

                        }
                    });*/
        }
    }

    /*@Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }*/

    private boolean validate(){
        Boolean hasil = false;

        String E_mail = email.getText().toString();
        String Pass = pass.getText().toString();

        if(E_mail.isEmpty() && Pass.isEmpty()){
            Toast.makeText(this,"Silahkan isi kembali", Toast.LENGTH_SHORT).show();
        }
        else{
            hasil = true;
        }
        return hasil;
    }
}
