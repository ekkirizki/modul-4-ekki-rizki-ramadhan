package com.kakike.modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class Daftar_Akun extends AppCompatActivity {

    private EditText Nama_Lengkap, Email, Password;
    private Button daftar;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar__akun);
        Nama_Lengkap = findViewById(R.id.Nama_Lengkap);
        Email = findViewById(R.id.Emailx);
        Password = findViewById(R.id.Passwordx);
        daftar = findViewById(R.id.Daftar);

        //firebase auth
        mAuth = FirebaseAuth.getInstance();

    }


    public void SignIn(){
        Intent SignIn = new Intent(this, Login.class);
        startActivity(SignIn);
    }

    public void login(View view) {
        SignIn();
    }
    public String pesan(String a){
        Toast.makeText(this, a, Toast.LENGTH_SHORT).show();
        return a;
    }

    public void Regis(View view) {
        if (check()) {
            String email,password, nama;
            email = Email.getText().toString().trim();
            password = Password.getText().toString().trim();

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                pesan("Regis Sukses");
                                FirebaseUser user = mAuth.getCurrentUser();
                                UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(Nama_Lengkap.getText().toString()).build();

                                user.updateProfile(userProfileChangeRequest);
                                startActivity(new Intent(Daftar_Akun.this, Login.class));
                                finish();
                            }
                            else{
                                pesan("Regis Gagal");
                            }
                        }
                    });
        }
    }

    public boolean check(){
        if (Nama_Lengkap.getText().toString().equals("")){
            Nama_Lengkap.setError("Masukan Nama");
            Nama_Lengkap.requestFocus();
            return false;
        }
        if (Email.getText().toString().equals("")){
            Email.setError("Masukan Email");
            Email.requestFocus();
            return false;
        }

        if (Password.getText().toString().equals("")){
            Password.setError("Masukan Password");
            Password.requestFocus();
            return false;
        }
        return true;
    }
}
